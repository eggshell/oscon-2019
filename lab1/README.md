# Lab 1: Running a Serverless Container

Since we saw that you can run a Google Cloud Run service in the TriggerMesh cloud thanks to Knative, we can test this via GitLab, using GitLab CI to deploy the Knative service.

For this, we are going to run a CI/CD pipeline which is going to use the TriggerMesh CLI `tm` to deploy a Google Cloud Run Hello container.

## GitLab CI Configuration

In the repository **root** directory view the `.gitlab-ci.yml` file:

![navigate_ciyaml](images/navigate_ciyaml.png)

Looking at the file, you can see that first we define a single stage in our pipeline, which is `deploy-function`:

```yaml
stages:
  - deploy-function
```

And then we define the job `deploy-hello-function` in the `deploy-function` stage, defining the `environment` we'll use, the container `image` to use, and the commands to run:

```yaml
deploy-hello-function:
  stage: deploy-function
  environment: test
  image: gcr.io/triggermesh/tm:latest
  before_script:
    - echo $TMCONFIG > tmconfig
  script:
    - tm --config ./tmconfig deploy service hello-gitlab -f gcr.io/cloudrun/hello --wait
```

Note that the `before_script` is creating a **tmconfig** file with some data which we've pre-loaded into each lab account. The data is am account speciic token to the TriggerMesh Cloud which we are deploying to.

The `script` is the main part of the job we are running. Here we are using the TriggerMesh CLI `tm`, with our config file, to deploy our function.

That's it! A super simple pipeline to deploy our function to the TriggerMesh Cloud.

## Run the pipeline

On the left-hand side bar, click on **CI/CD** -> **Pipelines**:

![navigate_pipelines_2](images/navigate_pipelines_2.png)

This will bring you to the **Pipelines** page where you can see all your pipelines for your project and their statuses (there are probably none right now). You can also manually start a new pipeline from this page, which is what we are going to do.

In the upper right corner of this page click on the green **Run Pipeline** button:

![run_pipeline_1](images/run_pipeline_1.png)

On the next screen leave the **Run for** set to `master` since we want to run the pipeline on our default branch called "master". Then click on the green **Run Pipeline** button:

![run_pipeline_2](images/run_pipeline_2.png)

This will bring you to the pipeline graph page for your new pipeline, which in this case has only one stage with one job in it. Click on the job to see the detailed job log:

![run_pipeline_3](images/run_pipeline_3.png)

In the job logs you might see some errors while things are spinning up. You can safely ignore those:

![run_pipeline_4](images/run_pipeline_4.png)

When the job completes, you will get some output that looks like the following. Copy your function's URL (in the second to last line) to your clipboard:

![run_pipeline_5](images/run_pipeline_5.png)

## Test the Function

To test out your function, open up a new browser at the URL
you copied from the job output log. The page will load for a few seconds while a
container spins up to run your function.

![cloudrun](images/cloudrun.png)

If your page does not load or you receive a different response, please raise
your hand and one of the workshop TA's will come by.

## Summary

So, what exactly did we do here?

1. Looked at a GitLab CI/CD pipeline configuration to deploy the Google CloudRun hello application to Knative on
   TriggerMesh cloud.
1. Ran the GitLab CI/CD pipeline to deploy your function to TriggerMesh cloud.
1. Invoked your newly deployed function. On invocation, TriggerMesh cloud created a Knative service similar to Google Cloud Run which started a Kubernetes Pod.
1. Once the service has not received any more traffic over a certain interval,
   the container is destroyed, which means your time service has scaled down
   from 1 to 0.

## Congratulations!!

![Celebration Tanuki](images/celebrate-tanuki.png)

## Proceed to Lab 2

Continue to [lab 2](../lab2/README.md)
